import { useHistory, useParams } from 'react-router';
import Button from '@material-ui/core/Button';
import { Box } from '@material-ui/core';

const Home = () => {

    const {nome} = useParams();
    const history = useHistory();
    const handleVoltar = () => {        
        history.push('/');
    }

    return (
        <>
            <p className="bem_vindo">Bem vindo, <b>{nome}</b>!</p>
            <img src="https://image.flaticon.com/icons/png/512/427/427687.png" alt="troféu"/>
            <Box mt="2rem">
                <Button variant="contained" color="primary" onClick={handleVoltar}>Voltar</Button>
            </Box>            
        </>
    );
}

export default Home;