import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { Box } from '@material-ui/core';

import * as yup from 'yup';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';

import { useHistory } from 'react-router';


const SignUp = ({nome, setNome}) => {

    const formSchema = yup.object().shape({
        nome: yup.string().required("Nome obrigatório").matches(/[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ]+$/, "O nome deve conter apenas letras"),
        email: yup.string().required("E-mail obrigatório").email("E-mail inválido"),
        senha: yup.string().required("Senha obrigatória").min(8, "Senha deve ter ao menos 8 caracteres").matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[$*&@#])/, "senha deve conter ao menos uma letra maiúscula e uma minúscula, um número e um caractere especial"),
        confirmSenha: yup.string().oneOf([yup.ref('senha')], "As senhas devem ser iguais").required("Confirmação de senha obrigatória"),
    });
        
    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm({
        resolver: yupResolver(formSchema)
    });
        
    const history = useHistory();
    const onSubmitFunction = (data) => history.push({pathname: `/${nome}/home`, state: data});

    return (
        <form className="formulario" onSubmit={handleSubmit(onSubmitFunction)}>
            <TextField
                className="textfield"
                type="text"
                {...register("nome")}
                value={nome}
                onChange={(e) => setNome(e.target.value)}
                label="Nome" 
                />
                <p className="yup_error">{errors.nome?.message}</p>
            <TextField
                className="textfield"
                {...register("email")}
                label="E-mail" 
                />
                <p className="yup_error">{errors.email?.message}</p>
            <TextField
                className="textfield"
                {...register("senha")}
                label="Senha"  
                />
                <p className="yup_error">{errors.senha?.message}</p>
            <TextField
                className="textfield"
                {...register("confirmSenha")}
                label="Confirmar senha" 
                />
                <p className="yup_error">{errors.confirmSenha?.message}</p>
            <Box mt="2rem">
                <Button type="submit" variant="contained" color="primary">Enviar</Button>
            </Box>            
        </form>
    );
}

export default SignUp;