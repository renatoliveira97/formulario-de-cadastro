import './App.css';
import { Switch, Route } from 'react-router-dom';
import { useState } from 'react';

import Home from './pages/home';
import SignUp from './pages/signUp';

const App = () => {

  const [nome, setNome] = useState("");

  return (
    <div className="App">
      <header className="App-header">
        <Switch>
          <Route exact path="/">
            <SignUp nome={nome} setNome={setNome}/>
          </Route>
          <Route path="/:nome/home">
            <Home/>
          </Route>
        </Switch>
      </header>
    </div>
  );
}

export default App;
